package com.test.consumeruser.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @create: 2019-06-30 01:56
 **/
@RestController
public class UserController {

    /**
     * 简单的http请求获取结果
     */
    @Autowired
    RestTemplate restTemplate;

    /**
     * 包含了拉取的所有服务信息
     */
    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/buy")
    public String buyTicket(String name) {
        //通过restTemplate模版的getForObject方法，通过http连接来获取结果。
        //第一个参数是提供者的地址，在注册中心(http://)中的服务名(PROVIDER-TICKET)内的请求地址(ticket)，返回的结果是字符串类型
        String forObject = restTemplate.getForObject("http://PROVIDER-TICKET/ticket", String.class);
        return name + "购买了" + forObject;
    }

    /**
     * 使用软编码的方式来动态获取提供方服务的地址及端口
     *
     * @param name
     * @return
     */
    @GetMapping("buyDis")
    public String buyTicketDiscovery(String name) {
        //通过discoveryClient来获取指定服务
        List<ServiceInstance> instances = discoveryClient.getInstances("provider-ticket");
        //该名称的服务只有一个，因为不是集群的方式
        ServiceInstance serviceInstance = instances.get(0);
        //动态获取服务id加服务端口，加请求地址来获取请求结果。在这里如果使用host(IP)或者InstanceId是无法获取到结果的
        String forObject = restTemplate.getForObject("http://" + serviceInstance.getServiceId() + ":" + serviceInstance.getPort() + "/ticket", String.class);
        return name + "购买了" + forObject;
    }
}
