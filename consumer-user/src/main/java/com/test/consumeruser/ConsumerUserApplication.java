package com.test.consumeruser;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@EnableDiscoveryClient //开启发现服务功能
@SpringBootApplication
@RibbonClient(name = "PROVIDER-TICKET",configuration = RandomRule.class) //第二种自定义的均衡策略
public class ConsumerUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerUserApplication.class, args);
    }

    @LoadBalanced() //使用负载均衡机制,如果发现有多个可用服务，那么采用轮询的机制
    @Bean //在这里注入RestTemplate便可以在controller类中使用这个模版
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * 自定义均衡策略，不使用默认的轮询的策略
     *
     * @return
     */
//    @Bean
//    public IRule iRule() {
//        //随机策略
//        return new RandomRule();
//    }
}
